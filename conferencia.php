<?php include_once "includes/templates/header.php"; ?>


<section class="seccion">
    <h2>La mejor conferencia de diseño web en español</h2>
    <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit.
        Corporis sequi odit architecto, alias error sunt ipsam? Placeat
        minus non in, nostrum voluptates, sapiente omnis repellat nisi
        sit tempora possimus ducimus?Lorem ipsum dolor sit amet,
        consectetur adipisicing elit. Accusamus aliquid architecto
        deserunt dicta dolor et provident repudiandae. Assumenda
        blanditiis debitis deleniti dignissimos, dolor fugit hic ipsum
        minus obcaecati odit quae!
    </p>
</section>
<section class="seccion contenedor">
    <h2>Galería de Fotos</h2>
    <div class="galeria">
        <a href="img/galeria/01.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/01.jpg">
        </a>
        <a href="img/galeria/02.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/02.jpg">
        </a>
        <a href="img/galeria/03.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/03.jpg">
        </a>
        <a href="img/galeria/04.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/04.jpg">
        </a>
        <a href="img/galeria/05.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/05.jpg">
        </a>
        <a href="img/galeria/06.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/06.jpg">
        </a>
        <a href="img/galeria/07.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/07.jpg">
        </a>
        <a href="img/galeria/08.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/08.jpg">
        </a>
        <a href="img/galeria/09.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/09.jpg">
        </a>
        <a href="img/galeria/10.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/10.jpg">
        </a>

    </div>
</section>

    <?php include_once "includes/templates/footer.php"; ?>
