<footer class="site-footer">
    <div class="contenedor clearfix">
        <div class="footer-informacion">
            <h3>Sobre <span>gdlwebcamp</span></h3>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing
                elit. Asperiores aspernatur corporis deserunt ducimus
                laboriosam, perferendis perspiciatis quod? Animi
                architecto aspernatur dolor molestiae, pariatur sint
                vitae? Autem error esse quam quisquam.
            </p>
        </div>
        <div class="ultimos-tweets">
            <h3>Últimos <span>tweets</span></h3>
            <ul>
                <li>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Commodi cum, eius expedita fugiat incidunt
                    nobis nostrum porro quas.
                </li>
                <li>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Commodi cum, eius expedita fugiat incidunt
                    nobis nostrum porro quas.
                </li>
                <li>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Commodi cum, eius expedita fugiat incidunt
                    nobis nostrum porro quas.
                </li>
            </ul>
        </div>
        <div class="menu">
            <h3>Redes <span>sociales</span></h3>
            <nav class="redes-sociales">
                <a href="#"
                ><i aria-hidden="true" class="fab fa-facebook-f"></i
                    ></a>
                <a href="#"><i class="fab fa-twitter"></i></a>
                <a href="#"><i class="fab fa-pinterest-p"></i></a>
                <a href="#"><i class="fab fa-youtube"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
            </nav>
        </div>
    </div>

    <p class="copyright">
        &copy; 2020 · Diseño realizado por: <span>Jesús Martínez</span>
    </p>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    window.jQuery ||
    document.write(
        '<script src="js/vendor/jquery-3.5.1.min.js"><\/script>'
    );
</script>
<script src="js/plugins.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.lettering.js"></script>
<script src="js/jquery.waypoints.js"></script>
<script src="leaflet/leaflet.js"></script>
<script src="js/main.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] ||
        (b[l] = function () {
            (b[l].q = b[l].q || []).push(arguments);
        });
        b[l].l = +new Date();
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = "https://www.google-analytics.com/analytics.js";
        r.parentNode.insertBefore(e, r);
    })(window, document, "script", "ga");
    ga("create", "UA-XXXXX-X", "auto");
    ga("send", "pageview");
</script>
</body>
</html>
