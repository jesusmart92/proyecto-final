<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8"/>
    <meta content="ie=edge" http-equiv="x-ua-compatible"/>
    <title></title>
    <meta content="" name="description"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>

    <link href="apple-touch-icon.png" rel="apple-touch-icon"/>
    <!-- Place favicon.ico in the root directory -->
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Oswald&family=PT+Sans:wght@700&display=swap"
        rel="stylesheet"
    />
    <link href="css/normalize.css" rel="stylesheet"/>
    <link href="css/all.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="leaflet/leaflet.css">
    <link href="css/main.css" rel="stylesheet"/>

    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">
    You are using an <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a> to
    improve your experience.
</p>
<![endif]-->

<header class="site-header">
    <div class="hero">
        <div class="contenido-header">
            <nav class="redes-sociales">
                <a href="#"
                ><i aria-hidden="true" class="fab fa-facebook-f"></i
                    ></a>
                <a href="#"><i class="fab fa-twitter"></i></a>
                <a href="#"><i class="fab fa-pinterest-p"></i></a>
                <a href="#"><i class="fab fa-youtube"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
            </nav>
            <div class="informacion-evento">
                <div class="clearfix">
                    <p class="fecha">
                        <i class="far fa-calendar-alt"></i> 15-17 Jun
                    </p>
                    <p class="ciudad">
                        <i class="fas fa-map-marker-alt"></i> Elche,
                        Alicante
                    </p>
                </div>

                <h1 class="nombre-sitio">GdlWebCamp</h1>
                <p class="slogan">
                    La mejor conferencia de <span>diseño web</span>
                </p>
            </div>
            <!--Informacion Evento-->
        </div>
    </div>
    <!--Hero-->
</header>

<div class="barra">
    <div class="contenedor clearfix">
        <div class="logo">
            <a href="index.php">
            <img alt="Logo gdlwebcam" src="/img/logo.svg"/>
            </a>
        </div>

        <div class="menu-movil">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <nav class="navegacion-principal">
            <a href="conferencia.php">Conferencia</a>
            <a href="calendiario.php">Calendario</a>
            <a href="invitados.php">Invitados</a>
            <a href="registro.php">Reservaciones</a>
        </nav>
    </div>
    <!--CONTENEDOR-->
</div>
<!--BARRA-->
