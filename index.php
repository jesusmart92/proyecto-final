<?php include_once "includes/templates/header.php"; ?>


<section class="seccion">
    <h2>La mejor conferencia de diseño web en español</h2>
    <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit.
        Corporis sequi odit architecto, alias error sunt ipsam? Placeat
        minus non in, nostrum voluptates, sapiente omnis repellat nisi
        sit tempora possimus ducimus?Lorem ipsum dolor sit amet,
        consectetur adipisicing elit. Accusamus aliquid architecto
        deserunt dicta dolor et provident repudiandae. Assumenda
        blanditiis debitis deleniti dignissimos, dolor fugit hic ipsum
        minus obcaecati odit quae!
    </p>
</section>
<!--seccion -->

<section class="programa">
    <div class="contenedor-video">
        <video autoplay loop muted poster="./img/bg-talleres.jpg">
            <source src="video/video.mp4" type="video/mp4"/>
            <source src="video/video.webm" type="video/webm"/>
            <source src="video/video.ogv" type="video/ogg"/>
        </video>
    </div>
    <!--contenedor video-->
    <div class="contenido-programa">
        <div class="contenedor">
            <div class="programa-evento">
                <h2>Programa del Evento</h2>
                <nav class="menu-programa">
                    <a href="#talleres"
                    ><i class="fas fa-code"></i> Talleres</a
                    >
                    <a href="#conferencias"
                    ><i class="fas fa-comments"></i> Conferencias</a
                    >
                    <a href="#seminarios"
                    ><i class="fas fa-university"></i> Seminarios</a
                    >
                </nav>
                <div class="info-curso ocultar clearfix" id="talleres">
                    <div class="detalle-evento">
                        <h3>HTML5, CSS3, JavaScript</h3>
                        <p><i class="far fa-clock"></i> 16:00 hrs</p>
                        <p>
                            <i class="far fa-calendar-alt"></i> 10 de
                            Dic
                        </p>
                        <p>
                            <i class="fas fa-user"></i> Jesús Martínez
                            Beltrán
                        </p>
                    </div>
                    <div class="detalle-evento">
                        <h3>Responsive Web Design</h3>
                        <p><i class="far fa-clock"></i> 20:00 hrs</p>
                        <p>
                            <i class="far fa-calendar-alt"></i> 10 de
                            Dic
                        </p>
                        <p>
                            <i class="fas fa-user"></i> Jesús Martínez
                            Beltrán
                        </p>
                    </div>
                    <a class="boton float-rigth" href="#">Ver todos</a>
                </div>
                <!--#talleres -->
                <div class="info-curso ocultar clearfix" id="conferencias">
                    <div class="detalle-evento">
                        <h3>Como ser Freelancer</h3>
                        <p><i class="far fa-clock"></i> 10:00 hrs</p>
                        <p>
                            <i class="far fa-calendar-alt"></i> 10 de
                            Dic
                        </p>
                        <p>
                            <i class="fas fa-user"></i> Manuel Victoria
                        </p>
                    </div>
                    <div class="detalle-evento">
                        <h3>Tecnologías del Futuro</h3>
                        <p><i class="far fa-clock"></i> 17:00 hrs</p>
                        <p>
                            <i class="far fa-calendar-alt"></i> 10 de
                            Dic
                        </p>
                        <p>
                            <i class="fas fa-user"></i> José Manuel Valcarcel
                        </p>
                    </div>
                    <a class="boton float-rigth" href="#">Ver todos</a>
                </div>
                <!--#talleres -->
                <div class="info-curso ocultar clearfix" id="seminarios">
                    <div class="detalle-evento">
                        <h3>Diseño UI/UX para móviles</h3>
                        <p><i class="far fa-clock"></i> 17:00 hrs</p>
                        <p>
                            <i class="far fa-calendar-alt"></i> 11 de
                            Dic
                        </p>
                        <p>
                            <i class="fas fa-user"></i> Jesús Martínez
                            Beltrán
                        </p>
                    </div>
                    <div class="detalle-evento">
                        <h3>Aprende a programar en una mañana</h3>
                        <p><i class="far fa-clock"></i> 10:00 hrs</p>
                        <p>
                            <i class="far fa-calendar-alt"></i> 11 de
                            Dic
                        </p>
                        <p>
                            <i class="fas fa-user"></i> Alejandro Perez
                        </p>
                    </div>
                    <a class="boton float-rigth" href="#">Ver todos</a>
                </div>
                <!--#talleres -->

            </div>
            <!--programa-evento -->
        </div>
        <!--contenedor -->
    </div>
    <!--contenido-programa-->
</section>
<!--programa-->
<section class="invitados contenedor seccion">
    <h2>Nuestros Invitados</h2>
    <ul class="lista-invitados clearfix">
        <li>
            <div class="invitado">
                <img alt="Invitado" src="img/invitado1.jpg"/>
                <p>Rafael Bautista</p>
            </div>
        </li>
        <li>
            <div class="invitado">
                <img alt="Invitado" src="img/invitado2.jpg"/>
                <p>Shari Herrera</p>
            </div>
        </li>
        <li>
            <div class="invitado">
                <img alt="Invitado" src="img/invitado3.jpg"/>
                <p>Gregorio Sanchez</p>
            </div>
        </li>
        <li>
            <div class="invitado">
                <img alt="Invitado" src="img/invitado4.jpg"/>
                <p>susana Rivera</p>
            </div>
        </li>
        <li>
            <div class="invitado">
                <img alt="Invitado" src="img/invitado5.jpg"/>
                <p>Harold Garcia</p>
            </div>
        </li>
        <li>
            <div class="invitado">
                <img alt="Invitado" src="img/invitado6.jpg"/>
                <p>Susan Sanchez</p>
            </div>
        </li>
    </ul>
</section>

<div class="contador parallax">
    <div class="contenedor">
        <ul class="resumen-evento clearfix">
            <li>
                <p class="numero">0</p>
                Invitados
            </li>
            <li>
                <p class="numero">0</p>
                Talleres
            </li>
            <li>
                <p class="numero">0</p>
                Dias
            </li>
            <li>
                <p class="numero">0</p>
                Conferencias
            </li>
        </ul>
    </div>
</div>

<section class="precios seccion">
    <h2>Precios</h2>
    <div class="contenedor">
        <ul class="lista-precios clearfix">
            <li>
                <div class="tabla-precio">
                    <h3>Pase por día</h3>
                    <p class="numero">30€</p>
                    <ul>
                        <li>Bocadillos Gratis</li>
                        <li>Todas las conferencias</li>
                        <li>Todos los talleres</li>
                    </ul>
                    <a class="boton hollow" href="#">Comprar</a>
                </div>
            </li>
            <li>
                <div class="tabla-precio">
                    <h3>Todos los días</h3>
                    <p class="numero">50€</p>
                    <ul>
                        <li>Bocadillos Gratis</li>
                        <li>Todas las conferencias</li>
                        <li>Todos los talleres</li>
                    </ul>
                    <a class="boton" href="#">Comprar</a>
                </div>
            </li>
            <li>
                <div class="tabla-precio">
                    <h3>Pase por 2 días</h3>
                    <p class="numero">45€</p>
                    <ul>
                        <li>Bocadillos Gratis</li>
                        <li>Todas las conferencias</li>
                        <li>Todos los talleres</li>
                    </ul>
                    <a class="boton hollow" href="#">Comprar</a>
                </div>
            </li>
        </ul>
    </div>
</section>

<div class="mapa" id="mapa">

</div>

<section class="seccion">
    <h2>Testimoniales</h2>
    <div class="testimoniales contenedor clearfix">
        <div class="testimonial">
            <blockquote>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Adipisci blanditiis dolore eveniet quisquam
                    vel. Accusantium aspernatur consequuntur dicta
                    doloremque dolores fugit minima non placeat porro
                    quia. Laboriosam minima nostrum quibusdam.
                </p>
                <footer class="info-testimonial clearfix">
                    <img
                        alt="Imagen Testimonial"
                        src="/img/testimonial.jpg"
                    />
                    <cite
                    >Oswaldo Aponte Escobedo
                        <span>Diseñador en @prisma</span></cite
                    >
                </footer>
            </blockquote>
        </div>
        <!--Testimonial -->
        <div class="testimonial">
            <blockquote>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Adipisci blanditiis dolore eveniet quisquam
                    vel. Accusantium aspernatur consequuntur dicta
                    doloremque dolores fugit minima non placeat porro
                    quia. Laboriosam minima nostrum quibusdam.
                </p>
                <footer class="info-testimonial clearfix">
                    <img
                        alt="Imagen Testimonial"
                        src="/img/testimonial.jpg"
                    />
                    <cite
                    >Oswaldo Aponte Escobedo
                        <span>Diseñador en @prisma</span></cite
                    >
                </footer>
            </blockquote>
        </div>
        <!--Testimonial -->
        <div class="testimonial">
            <blockquote>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Adipisci blanditiis dolore eveniet quisquam
                    vel. Accusantium aspernatur consequuntur dicta
                    doloremque dolores fugit minima non placeat porro
                    quia. Laboriosam minima nostrum quibusdam.
                </p>
                <footer class="info-testimonial clearfix">
                    <img
                        alt="Imagen Testimonial"
                        src="/img/testimonial.jpg"
                    />
                    <cite
                    >Oswaldo Aponte Escobedo
                        <span>Diseñador en @prisma</span></cite
                    >
                </footer>
            </blockquote>
        </div>
        <!--Testimonial -->
    </div>
</section>

<div class="newsletter parallax">
    <div class="contenido contenedor">
        <p>Regístrate al newsletter</p>
        <h3>gdlwebcamp</h3>
        <a class="boton transparente" href="#">Registro</a>
    </div>
    <!--contenido-->
</div>
<!--newsletter -->

<section class="seccion">
    <h2>Faltan</h2>
    <div class="cuenta-regresiva contenedor">
        <ul class="clearfix">
            <li>
                <p id="dias" class="numero"></p>
                días
            </li>
            <li>
                <p id="horas" class="numero"></p>
                horas
            </li>
            <li>
                <p id="minutos" class="numero"></p>
                minutos
            </li>
            <li>
                <p id="segundos" class="numero"></p>
                segundos
            </li>
        </ul>
    </div>
</section>

<?php include_once "includes/templates/footer.php"; ?>
