var map = L.map('mapa').setView([38.264686, -0.71151], 16);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([38.264686, -0.71151]).addTo(map)
    .bindPopup('Esta es mi **** casa.')
    .openPopup()
