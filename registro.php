<?php include_once "includes/templates/header.php"; ?>

<section class="seccion contenedor">
    <h2>Registro de Usuarios</h2>
    <form action="index.php" class="registro" id="registro" method="post">
        <div class="registro caja clearfix" id="datos_usuario">
            <div class="campo">
                <label for="nombre">Nombre: </label>
                <input id="nombre" name="nombre" placeholder="Tu Nombre" type="text">
            </div>
            <div class="campo">
                <label for="apellido">Apellido: </label>
                <input id="apellido" name="apellido" placeholder="Tu Apellido" type="text">
            </div>
            <div class="campo">
                <label for="email">Email: </label>
                <input id="email" name="email" placeholder="Tu Email" type="email">
            </div>
            <div id="error"></div>
        </div><!-- Datos usuario -->
        <div class="paquetes" id="paquetes">
            <h3>Elige el número de boletos</h3>
            <ul class="lista-precios clearfix">
                <li>
                    <div class="tabla-precio">
                        <h3>Pase por día (viernes)</h3>
                        <p class="numero">30€</p>
                        <ul>
                            <li>Bocadillos Gratis</li>
                            <li>Todas las conferencias</li>
                            <li>Todos los talleres</li>
                        </ul>
                        <div class="orden">
                            <label for="pase_dia">Boletos deseados:</label>
                            <input id="pase_dia" min="0" placeholder="0" size="3" type="number">
                        </div>
                    </div>
                </li>
                <li>
                    <div class="tabla-precio">
                        <h3>Todos los días</h3>
                        <p class="numero">50€</p>
                        <ul>
                            <li>Bocadillos Gratis</li>
                            <li>Todas las conferencias</li>
                            <li>Todos los talleres</li>
                        </ul>
                        <div class="orden">
                            <label for="pase_completo">Boletos deseados:</label>
                            <input id="pase_completo" min="0" placeholder="0" size="3" type="number">
                        </div>
                    </div>
                </li>
                <li>
                    <div class="tabla-precio">
                        <h3>Pase por 2 días(viernes y sábado)</h3>
                        <p class="numero">45€</p>
                        <ul>
                            <li>Bocadillos Gratis</li>
                            <li>Todas las conferencias</li>
                            <li>Todos los talleres</li>
                        </ul>
                        <div class="orden">
                            <label for="pase_dosdias">Boletos deseados:</label>
                            <input id="pase_dosdias" min="0" placeholder="0" size="3" type="number">
                        </div>
                    </div>
                </li>
            </ul>
        </div><!-- paquetes -->

        <div class="eventos clearfix" id="eventos">
            <h3>Elige tus talleres</h3>
            <div class="caja">
                <div class="contenido-dia clearfix" id="viernes">
                    <h4>Viernes</h4>
                    <div>
                        <p>Talleres:</p>
                        <label><input id="taller_01" name="registro[]" type="checkbox" value="taller_01">
                            <time>10:00</time>
                            Responsive Web Design</label>
                        <label><input id="taller_02" name="registro[]" type="checkbox" value="taller_02">
                            <time>12:00</time>
                            Flexbox</label>
                        <label><input id="taller_03" name="registro[]" type="checkbox" value="taller_03">
                            <time>14:00</time>
                            HTML5 y CSS3</label>
                        <label><input id="taller_04" name="registro[]" type="checkbox" value="taller_04">
                            <time>17:00</time>
                            Drupal</label>
                        <label><input id="taller_05" name="registro[]" type="checkbox" value="taller_05">
                            <time>19:00</time>
                            WordPress</label>
                    </div>
                    <div>
                        <p>Conferencias:</p>
                        <label><input id="conf_01" name="registro[]" type="checkbox" value="conf_01">
                            <time>10:00</time>
                            Como ser Freelancer</label>
                        <label><input id="conf_02" name="registro[]" type="checkbox" value="conf_02">
                            <time>17:00</time>
                            Tecnologías del Futuro</label>
                        <label><input id="conf_03" name="registro[]" type="checkbox" value="conf_03">
                            <time>19:00</time>
                            Seguridad en la Web</label>
                    </div>
                    <div>
                        <p>Seminarios:</p>
                        <label><input id="sem_01" name="registro[]" type="checkbox" value="sem_01">
                            <time>10:00</time>
                            Diseño UI y UX para móviles</label>
                    </div>
                </div> <!--#viernes-->
                <div class="contenido-dia clearfix" id="sabado">
                    <h4>Sábado</h4>
                    <div>
                        <p>Talleres:</p>
                        <label><input id="taller_06" name="registro[]" type="checkbox" value="taller_06">
                            <time>10:00</time>
                            AngularJS</label>
                        <label><input id="taller_07" name="registro[]" type="checkbox" value="taller_07">
                            <time>12:00</time>
                            PHP y MySQL</label>
                        <label><input id="taller_08" name="registro[]" type="checkbox" value="taller_08">
                            <time>14:00</time>
                            JavaScript Avanzado</label>
                        <label><input id="taller_09" name="registro[]" type="checkbox" value="taller_09">
                            <time>17:00</time>
                            SEO en Google</label>
                        <label><input id="taller_10" name="registro[]" type="checkbox" value="taller_10">
                            <time>19:00</time>
                            De Photoshop a HTML5 y CSS3</label>
                        <label><input id="taller_11" name="registro[]" type="checkbox" value="taller_11">
                            <time>21:00</time>
                            PHP Medio y Avanzado</label>
                    </div>
                    <div>
                        <p>Conferencias:</p>
                        <label><input id="conf_04" name="registro[]" type="checkbox" value="conf_04">
                            <time>10:00</time>
                            Como crear una tienda online que venda millones en pocos días</label>
                        <label><input id="conf_05" name="registro[]" type="checkbox" value="conf_05">
                            <time>17:00</time>
                            Los mejores lugares para encontrar trabajo</label>
                        <label><input id="conf_06" name="registro[]" type="checkbox" value="conf_06">
                            <time>19:00</time>
                            Pasos para crear un negocio rentable</label>
                    </div>
                    <div>
                        <p>Seminarios:</p>
                        <label><input id="sem_02" name="registro[]" type="checkbox" value="sem_02">
                            <time>10:00</time>
                            Aprende a Programar en una mañana</label>
                        <label><input id="sem_03" name="registro[]" type="checkbox" value="sem_03">
                            <time>17:00</time>
                            Diseño UI y UX para móviles</label>
                    </div>
                </div> <!--#sabado-->
                <div class="contenido-dia clearfix" id="domingo">
                    <h4>Domingo</h4>
                    <div>
                        <p>Talleres:</p>
                        <label><input id="taller_12" name="registro[]" type="checkbox" value="taller_12">
                            <time>10:00</time>
                            Laravel</label>
                        <label><input id="taller_13" name="registro[]" type="checkbox" value="taller_13">
                            <time>12:00</time>
                            Crea tu propia API</label>
                        <label><input id="taller_14" name="registro[]" type="checkbox" value="taller_14">
                            <time>14:00</time>
                            JavaScript y jQuery</label>
                        <label><input id="taller_15" name="registro[]" type="checkbox" value="taller_15">
                            <time>17:00</time>
                            Creando Plantillas para WordPress</label>
                        <label><input id="taller_16" name="registro[]" type="checkbox" value="taller_16">
                            <time>19:00</time>
                            Tiendas Virtuales en Magento</label>
                    </div>
                    <div>
                        <p>Conferencias:</p>
                        <label><input id="conf_07" name="registro[]" type="checkbox" value="conf_07">
                            <time>10:00</time>
                            Como hacer Marketing en línea</label>
                        <label><input id="conf_08" name="registro[]" type="checkbox" value="conf_08">
                            <time>17:00</time>
                            ¿Con que lenguaje debo empezar?</label>
                        <label><input id="conf_09" name="registro[]" type="checkbox" value="conf_09">
                            <time>19:00</time>
                            Frameworks y librerias Open Source</label>
                    </div>
                    <div>
                        <p>Seminarios:</p>
                        <label><input id="sem_04" name="registro[]" type="checkbox" value="sem_04">
                            <time>14:00</time>
                            Creando una App en Android en una tarde</label>
                        <label><input id="sem_05" name="registro[]" type="checkbox" value="sem_05">
                            <time>17:00</time>
                            Creando una App en iOS en una tarde</label>
                    </div>
                </div> <!--#domingo-->
            </div><!--.caja-->
        </div> <!--#eventos-->

        <div class="resumen clearfix" id="resumen">
            <h3>Pago y Extras</h3>
            <div class="caja clearfix">
                <div class="extras">
                    <div class="orden">
                        <label for="camisa_evento">Camisa del Evento 10€ <small>(promocion 7% dto.)</small></label>
                        <input id="camisa_evento" min="0" placeholder="0" size="3" type="number">
                    </div><!--orden -->
                    <label for="etiquetas">Paquete de 10 etiquetas 2€ <small>(HTML%, CSS3, JavaScript,
                        Chrome)</small></label>
                    <input id="etiquetas" min="0" placeholder="0" size="3" type="number">

                    <div class="orden">
                        <label for="regalo">Seleccione un regalo</label> <br/>
                        <select id="regalo" required>
                            <option value="">-- Seleccione un regalo --</option>
                            <option value="ETI">Etiquetas</option>
                            <option value="PUL">Pulsera</option>
                            <option value="PLU">Plumas</option>
                        </select>
                    </div><!-- orden -->
                    <input class="boton" id="calcular" type="button" value="calcular">
                </div><!-- extras -->
                <div class="total">
                    <p>Resumen:</p>
                    <div id="lista-productos">

                    </div>
                    <p>Total:</p>
                    <div id="suma-total">
                        <input class="boton" id="btnRegistro" type="submit" value="Pagar">
                    </div><!-- total -->
                </div> <!-- caja -->
            </div><!-- resumen -->
        </div>
    </form>
</section>



<?php include_once "includes/templates/footer.php"; ?>
